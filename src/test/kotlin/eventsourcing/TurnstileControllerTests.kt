package eventsourcing

import com.ninjasquad.springmockk.MockkBean
import eventsourcing.controller.TurnstileController
import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.Subscription
import eventsourcing.domain.TurnstileEvent
import eventsourcing.repository.TurnstileEventRepository
import eventsourcing.service.SubscriptionService
import eventsourcing.service.TurnstileService
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.LocalDateTime

@WebMvcTest(TurnstileController::class)
class TurnstileControllerTests(@Autowired val mockMvc: MockMvc) {
    @MockkBean
    private lateinit var turnstileEventRepository: TurnstileEventRepository

    @MockkBean
    private lateinit var subscriptionService: SubscriptionService

    @TestConfiguration
    class Config(
        val turnstileEventRepository: TurnstileEventRepository,
        val subscriptionService: SubscriptionService,
    ) {
        @Bean
        fun turnstileService() = TurnstileService(subscriptionService, turnstileEventRepository)
    }

    @Test
    fun `Does not allow expired subscription`() {
        val userPhoneValue = "999 999 9999"
        val userPhone = LocalPhoneNumber(userPhoneValue)
        every { turnstileEventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhone) }
            .returns(null)
        every { subscriptionService.findSubscription(userPhone) }
            .returns(Subscription(userPhone, LocalDateTime.MIN))
        mockMvc.perform(get("/turnstile/$userPhoneValue/enter"))
            .andExpect(status().isUnprocessableEntity)
    }

    @Test
    fun `Does not allow double enters`() {
        val userPhoneValue = "999 999 9999"
        val userPhone = LocalPhoneNumber(userPhoneValue)
        every { subscriptionService.findSubscription(userPhone) }
            .returns(Subscription(userPhone, LocalDateTime.MAX))
        every { turnstileEventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhone) }
            .returns(TurnstileEvent.Entered(userPhone, 0))
        mockMvc.perform(get("/turnstile/$userPhoneValue/enter"))
            .andExpect(status().isUnprocessableEntity)
    }

    @Test
    fun `Does not allow double exits`() {
        val userPhoneValue = "999 999 9999"
        val userPhone = LocalPhoneNumber(userPhoneValue)
        every { subscriptionService.findSubscription(userPhone) }
            .returns(Subscription(userPhone, LocalDateTime.MAX))
        every { turnstileEventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhone) }
            .returns(TurnstileEvent.Exited(userPhone, 0))
        mockMvc.perform(get("/turnstile/$userPhoneValue/exit"))
            .andExpect(status().isUnprocessableEntity)
    }

    @Test
    fun `Enter then Exit works`() {
        val userPhoneValue = "999 999 9999"
        val userPhone = LocalPhoneNumber(userPhoneValue)
        every { subscriptionService.findSubscription(userPhone) }
            .returns(Subscription(userPhone, LocalDateTime.MAX))
        every { turnstileEventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhone) }
            .returns(TurnstileEvent.Entered(userPhone, 0))
        every { turnstileEventRepository.save(any()) }
            .returns(TurnstileEvent.Exited())
        mockMvc.perform(get("/turnstile/$userPhoneValue/exit"))
            .andExpect(status().isOk)
            .andExpect(content().string(""))
    }
}