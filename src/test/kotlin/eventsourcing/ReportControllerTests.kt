package eventsourcing

import com.ninjasquad.springmockk.MockkBean
import eventsourcing.controller.ReportController
import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.TurnstileEvent
import eventsourcing.repository.TurnstileEventRepository
import eventsourcing.service.ReportService
import io.mockk.CapturingSlot
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.temporal.TemporalAmount
import java.util.concurrent.ScheduledExecutorService

@WebMvcTest(ReportController::class)
class ReportControllerTests(@Autowired val mockMvc: MockMvc) {
    @MockkBean
    private lateinit var turnstileEventRepository: TurnstileEventRepository

    @MockkBean
    private lateinit var executor: ScheduledExecutorService

    @Autowired
    private lateinit var sweepingTaskCapture: CapturingSlot<Runnable>

    @TestConfiguration
    class Config(
        val turnstileEventRepository: TurnstileEventRepository,
        val executor: ScheduledExecutorService,
    ) {
        @Bean
        fun reportService(sweepingTaskCapture: CapturingSlot<Runnable>): ReportService {
            every { executor.scheduleWithFixedDelay(capture(sweepingTaskCapture), any(), any(), any()) }
                .returns(null)
            return ReportService(turnstileEventRepository, executor)
        }

        @Bean
        fun sweepingTaskCapture() = slot<Runnable>()
    }

    @Test
    fun `Can update statistics`() {
        every { turnstileEventRepository.findByTimeCreatedBetweenOrderByTimeCreated(any(), any()) }
            .returns(listOf())
        sweepingTaskCapture.captured.run()
        mockMvc.perform(get("/report/perDay").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(jsonPath("\$").isEmpty)
        mockMvc.perform(get("/report/averages").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().string(""))

        val userPhone = LocalPhoneNumber("999 999 9999")
        val makeVisit = { start: LocalDateTime, duration: TemporalAmount ->
            val enter = mockk<TurnstileEvent.Entered>()
            every { enter.timeCreated } returns start
            every { enter.userPhoneNumber } returns userPhone
            val exit = mockk<TurnstileEvent.Exited>()
            every { exit.timeCreated } returns start + duration
            every { exit.userPhoneNumber } returns userPhone
            enter to exit
        }

        val day1 = LocalDate.parse("2007-12-03").atStartOfDay()
        val (enter1, exit1) = makeVisit(day1, Duration.ofHours(1))
        val day2 = day1 + Period.ofDays(1)
        val (enter2, exit2) = makeVisit(day2, Duration.ofHours(2))
        val (enter3, exit3) = makeVisit(day2 + Duration.ofMinutes(90), Duration.ofMinutes(90))

        every { turnstileEventRepository.findByTimeCreatedBetweenOrderByTimeCreated(any(), any()) }
            .returns(listOf(enter1, exit1, enter2, exit2, enter3, exit3))

        sweepingTaskCapture.captured.run()
        mockMvc.perform(get("/report/perDay").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(jsonPath("\$.${day1.toLocalDate()}.totalDuration").value(Duration.ofHours(1).toString()))
            .andExpect(jsonPath("\$.${day1.toLocalDate()}.nvisitations").value(1))
            .andExpect(jsonPath("\$.${day2.toLocalDate()}.totalDuration").value(Duration.ofMinutes(210).toString()))
            .andExpect(jsonPath("\$.${day2.toLocalDate()}.nvisitations").value(2))

        mockMvc.perform(get("/report/averages").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(jsonPath("\$.averageVisitations").value(1.5))
            .andExpect(jsonPath("\$.averageVisitDuration").value(Duration.ofMinutes(90).toString()))
    }

}