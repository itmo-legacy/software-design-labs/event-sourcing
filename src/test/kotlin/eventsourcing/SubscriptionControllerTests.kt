package eventsourcing

import com.ninjasquad.springmockk.MockkBean
import eventsourcing.controller.SubscriptionController
import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.SubscriptionEvent
import eventsourcing.repository.SubscriptionEventRepository
import eventsourcing.service.SubscriptionService
import io.mockk.every
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.time.Period

@WebMvcTest(SubscriptionController::class)
class SubscriptionControllerTests(@Autowired val mockMvc: MockMvc) {
    @MockkBean
    private lateinit var subscriptionEventRepository: SubscriptionEventRepository

    @TestConfiguration
    class Config(val subscriptionEventRepository: SubscriptionEventRepository) {
        @Bean
        fun subscriptionService() = SubscriptionService(subscriptionEventRepository)
    }

    @Test
    fun `Does not let invalid phone numbers in`() {
        mockMvc.perform(get("/subscription/99 999 999/view").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest)
    }

    @Test
    fun `Shows no subscriptions when a user is not registered`() {
        every {
            subscriptionEventRepository.findByUserPhoneNumberOrderByTimeCreated(any())
        } returns emptyList()
        mockMvc.perform(get("/subscription/999 999 9999/view").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().string(""))
    }

    @Test
    fun `Shows a subscription when it exists`() {
        val userPhoneValue = "999 999 9999"
        val userPhone = LocalPhoneNumber(userPhoneValue)
        val initialPeriod = Period.ofDays(30)
        every {
            subscriptionEventRepository.findByUserPhoneNumberOrderByTimeCreated(userPhone)
        } returns listOf(SubscriptionEvent.Created(userPhone, initialPeriod))
        mockMvc.perform(get("/subscription/$userPhoneValue/view").accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk)
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("\$.userPhoneNumber.phoneNumber").value(userPhoneValue))
    }
}