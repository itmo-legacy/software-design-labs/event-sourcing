package eventsourcing.repository

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.SubscriptionEvent
import org.springframework.data.jpa.repository.JpaRepository

interface SubscriptionEventRepository: JpaRepository<SubscriptionEvent, Long> {
    fun findByUserPhoneNumberOrderByTimeCreated(userPhoneNumber: LocalPhoneNumber): List<SubscriptionEvent>

    fun findFirstByUserPhoneNumberOrderByVersionDesc(userPhoneNumber: LocalPhoneNumber): SubscriptionEvent?
}