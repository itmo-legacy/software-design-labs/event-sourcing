package eventsourcing.repository

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.TurnstileEvent
import org.springframework.data.jpa.repository.JpaRepository
import java.time.LocalDateTime

interface TurnstileEventRepository : JpaRepository<TurnstileEvent, Long> {
    fun findFirstByUserPhoneNumberOrderByVersionDesc(userPhoneNumber: LocalPhoneNumber): TurnstileEvent?

    fun findByTimeCreatedBetweenOrderByTimeCreated(
        after: LocalDateTime,
        until: LocalDateTime,
    ): List<TurnstileEvent>
}