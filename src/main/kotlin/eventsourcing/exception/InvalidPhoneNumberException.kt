package eventsourcing.exception

class InvalidPhoneNumberException(offendingNumber: String) :
    Exception("The phone number ($offendingNumber) does not conform to the format (xxx xxx xxxx)")