package eventsourcing.exception

class SubscriptionException(message: String, cause: Exception? = null) :
    Exception(message, cause)