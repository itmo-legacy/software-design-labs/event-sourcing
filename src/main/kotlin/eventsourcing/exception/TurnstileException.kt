package eventsourcing.exception

import eventsourcing.domain.LocalPhoneNumber

class TurnstileException(userPhoneNumber: LocalPhoneNumber, message: String, cause: Exception? = null) :
    Exception("Issue with ${userPhoneNumber.phoneNumber}: $message", cause)