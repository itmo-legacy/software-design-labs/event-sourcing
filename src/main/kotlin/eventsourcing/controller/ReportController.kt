package eventsourcing.controller

import eventsourcing.service.ReportService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/report")
class ReportController(val reportService: ReportService) {
    @GetMapping("/perDay")
    fun showPerDayInfo() = reportService.getInfoPerDay()

    @GetMapping("/averages")
    fun showAverages() = reportService.getAverages()
}