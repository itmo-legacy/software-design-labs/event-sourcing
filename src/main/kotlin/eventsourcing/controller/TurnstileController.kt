package eventsourcing.controller

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.service.TurnstileService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/turnstile/{unvalidatedUserPhoneNumber}/")
class TurnstileController(val turnstileService: TurnstileService) {
    @GetMapping("/enter")
    fun enter(@PathVariable unvalidatedUserPhoneNumber: String) {
        val userPhoneNumber = LocalPhoneNumber(unvalidatedUserPhoneNumber)
        turnstileService.registerEnter(userPhoneNumber)
    }

    @GetMapping("/exit")
    fun exit(@PathVariable unvalidatedUserPhoneNumber: String) {
        val userPhoneNumber = LocalPhoneNumber(unvalidatedUserPhoneNumber)
        turnstileService.registerExit(userPhoneNumber)
    }
}