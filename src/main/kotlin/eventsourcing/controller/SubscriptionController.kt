package eventsourcing.controller

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.Subscription
import eventsourcing.service.SubscriptionService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.time.Period

@RestController
@RequestMapping("/subscription/{unvalidatedUserPhoneNumber}/")
class SubscriptionController(val subscriptionService: SubscriptionService) {
    @GetMapping("/view")
    fun view(@PathVariable unvalidatedUserPhoneNumber: String): Subscription? {
        val userPhoneNumber = LocalPhoneNumber(unvalidatedUserPhoneNumber)
        return subscriptionService.findSubscription(userPhoneNumber)
    }

    @GetMapping("/create")
    fun create(
        @PathVariable unvalidatedUserPhoneNumber: String,
        @RequestParam("durationInDays") durationInDays: Int,
    ) {
        validateDuration(durationInDays)
        val userPhoneNumber = LocalPhoneNumber(unvalidatedUserPhoneNumber)
        subscriptionService.createSubscription(userPhoneNumber, Period.ofDays(durationInDays))
    }

    @GetMapping("/extend")
    fun extend(
        @PathVariable unvalidatedUserPhoneNumber: String,
        @RequestParam("durationInDays") durationInDays: Int,
    ) {
        validateDuration(durationInDays)
        val userPhoneNumber = LocalPhoneNumber(unvalidatedUserPhoneNumber)
        subscriptionService.extendSubscription(userPhoneNumber, Period.ofDays(durationInDays))
    }

    private fun validateDuration(durationInDays: Int) {
        if (durationInDays <= 0) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Number of days should be positive")
        }
    }
}