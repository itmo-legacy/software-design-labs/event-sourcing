package eventsourcing

import eventsourcing.exception.InvalidPhoneNumberException
import eventsourcing.exception.SubscriptionException
import eventsourcing.exception.TurnstileException
import org.springframework.dao.DataIntegrityViolationException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
class RestResponseEntityExceptionHandler : ResponseEntityExceptionHandler() {
    @ExceptionHandler(SubscriptionException::class, TurnstileException::class)
    fun handleEventIssues(ex: Exception, request: WebRequest) =
        handleExceptionInternal(ex, ex.message, HttpHeaders.EMPTY, HttpStatus.UNPROCESSABLE_ENTITY, request)

    @ExceptionHandler(InvalidPhoneNumberException::class)
    fun handleInvalidPhoneNumber(ex: InvalidPhoneNumberException, request: WebRequest) =
        handleExceptionInternal(ex, ex.message, HttpHeaders.EMPTY, HttpStatus.BAD_REQUEST, request)

    @ExceptionHandler(DataIntegrityViolationException::class)
    fun handleConstraintViolation(ex: DataIntegrityViolationException, request: WebRequest) =
        handleExceptionInternal(
            ex,
            "There already is such entity (or it violates other constraints)",
            HttpHeaders.EMPTY,
            HttpStatus.CONFLICT,
            request,
        )
}