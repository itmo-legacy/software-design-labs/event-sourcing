package eventsourcing.domain

data class User(
    val id: Long,
    val name: String,
    val phoneNumber: LocalPhoneNumber,
    val subscriptionId: Long,
)