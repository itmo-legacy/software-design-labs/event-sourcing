package eventsourcing.domain

import java.time.LocalDateTime
import java.time.Period
import javax.persistence.*

@Entity
@Inheritance
@Table(
    indexes = [
        Index(columnList = "timeCreated DESC"),
        Index(columnList = "userPhoneNumber"),
    ],
    uniqueConstraints = [UniqueConstraint(columnNames = ["userPhoneNumber", "version"])],
)
sealed class SubscriptionEvent(
    // 12 symbols are needed currently to store a number, the rest is reserved for format changes.
    @Embedded
    @AttributeOverride(name = "phoneNumber", column = Column(name = "userPhoneNumber", length = 25))
    open val userPhoneNumber: LocalPhoneNumber,
    open val version: Long,
    open val timeCreated: LocalDateTime = LocalDateTime.now(),
    @Id
    @GeneratedValue
    open val id: Long = 0,
) {
    @Entity
    class Created(
        userPhoneNumber: LocalPhoneNumber,
        val initialPeriod: Period,
    ) : SubscriptionEvent(userPhoneNumber, version = 0) {
        constructor() : this(LocalPhoneNumber(), Period.ZERO)
    }

    @Entity
    class Extended(
        userPhoneNumber: LocalPhoneNumber,
        val extensionPeriod: Period,
        version: Long,
    ) : SubscriptionEvent(userPhoneNumber, version) {
        constructor() : this(LocalPhoneNumber(), Period.ZERO, 0)
    }
}