package eventsourcing.domain

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Inheritance
@Table(
    indexes = [
        Index(columnList = "timeCreated DESC"),
        Index(columnList = "userPhoneNumber"),
    ],
    uniqueConstraints = [UniqueConstraint(columnNames = ["userPhoneNumber", "version"])],
)
sealed class TurnstileEvent(
    // 12 symbols are needed currently to store a number, the rest is reserved for format changes.
    @Embedded
    @AttributeOverride(name = "phoneNumber", column = Column(name = "userPhoneNumber", length = 25))
    open val userPhoneNumber: LocalPhoneNumber,
    open val version: Long,
    open val timeCreated: LocalDateTime = LocalDateTime.now(),
    @Id
    @GeneratedValue
    open val id: Long = 0,
) {
    @Entity
    class Entered(userPhoneNumber: LocalPhoneNumber, version: Long) : TurnstileEvent(userPhoneNumber, version) {
        constructor() : this(LocalPhoneNumber(), 0)
    }
    @Entity
    class Exited(userPhoneNumber: LocalPhoneNumber, version: Long) : TurnstileEvent(userPhoneNumber, version) {
        constructor() : this(LocalPhoneNumber(), 0)
    }
}