package eventsourcing.domain

import eventsourcing.exception.InvalidPhoneNumberException
import javax.persistence.Embeddable

val PHONE_NUMBER_REGEX = """\d{3} \d{3} \d{4}""".toRegex()

/**
 * A representation of local phone numbers, i.e. without a country code.
 *
 * Should be in the form of "910 630 4585".
 */

@Embeddable
data class LocalPhoneNumber(val phoneNumber: String = "000 000 0000") {
    init {
        val matches = PHONE_NUMBER_REGEX.matches(phoneNumber)
        if (!matches) {
            throw InvalidPhoneNumberException(phoneNumber)
        }
    }
}