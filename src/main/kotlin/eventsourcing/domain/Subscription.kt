package eventsourcing.domain

import java.time.LocalDateTime

data class Subscription(
    val userPhoneNumber: LocalPhoneNumber,
    val expirationTime: LocalDateTime,
)