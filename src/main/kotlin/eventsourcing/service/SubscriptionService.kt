package eventsourcing.service

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.Subscription
import eventsourcing.domain.SubscriptionEvent
import eventsourcing.exception.SubscriptionException
import eventsourcing.repository.SubscriptionEventRepository
import org.springframework.stereotype.Component
import java.time.Period
import java.time.temporal.TemporalAmount

@Component
class SubscriptionService(private val eventRepository: SubscriptionEventRepository) {
    fun findSubscription(userPhoneNumber: LocalPhoneNumber): Subscription? {
        val events = eventRepository.findByUserPhoneNumberOrderByTimeCreated(userPhoneNumber)

        val getExtensionPeriod: ((SubscriptionEvent) -> TemporalAmount) =
            { event ->
                when (event) {
                    is SubscriptionEvent.Created -> event.initialPeriod
                    is SubscriptionEvent.Extended -> event.extensionPeriod
                }
            }

        return events.firstOrNull()?.timeCreated
            ?.let { initialTime ->
                events.fold(initialTime) { acc, event ->
                    maxOf(acc, event.timeCreated) + getExtensionPeriod(event)
                }
            }
            ?.let { expirationTime -> Subscription(userPhoneNumber, expirationTime) }
    }

    fun createSubscription(userPhoneNumber: LocalPhoneNumber, initialPeriod: Period) {
        val event = SubscriptionEvent.Created(userPhoneNumber, initialPeriod)
        eventRepository.save(event)
    }

    fun extendSubscription(userPhoneNumber: LocalPhoneNumber, extensionPeriod: Period) {
        val lastEvent = eventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhoneNumber)
            ?: throw SubscriptionException("Can't extend a subscription, that hasn't been created")
        val event = SubscriptionEvent.Extended(userPhoneNumber, extensionPeriod, lastEvent.version + 1)
        eventRepository.save(event)
    }
}