package eventsourcing.service

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.TurnstileEvent
import eventsourcing.repository.TurnstileEventRepository
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

@Component
final class ReportService(
    private val turnstileEventRepository: TurnstileEventRepository,
    sweepExecutor: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor(),
) {
    private final val infoPerDay = ConcurrentHashMap<LocalDate, DayInfo>()
    private final var lastDbSweepTime = LocalDateTime.now() - Period.ofYears(1)

    init {
        sweepExecutor.scheduleWithFixedDelay(this::sweepDb, 0, 1, TimeUnit.HOURS)
    }

    fun getInfoPerDay(): Map<LocalDate, DayInfo> = Collections.unmodifiableMap(infoPerDay)

    fun getAverages() = infoPerDay
        .reduceValues(100_000, DayInfo::combine)
        ?.let { combined ->
            val daysCount = infoPerDay.size
            Averages(
                averageVisitations = combined.nVisitations / daysCount.toDouble(),
                averageVisitDuration = combined.totalDuration.dividedBy(combined.nVisitations),
            )
        }

    private fun sweepDb() {
        synchronized(lastDbSweepTime) {
            val now = LocalDateTime.now()
            val lastTime = lastDbSweepTime
            lastDbSweepTime = now
            now to lastTime
        }.let { (now, lastTime) ->
            turnstileEventRepository
                .findByTimeCreatedBetweenOrderByTimeCreated(lastTime, now)
                .groupBy { it.timeCreated.toLocalDate() }
                .forEach { (day, events) ->
                    val dayInfo = DayInfo.fromOrderedDayEvents(events)
                    infoPerDay.merge(day, dayInfo, DayInfo::combine)
                }
        }
    }
}

data class DayInfo(val nVisitations: Long, val totalDuration: Duration) {
    fun combine(other: DayInfo) = DayInfo(
        nVisitations + other.nVisitations,
        totalDuration + other.totalDuration,
    )

    companion object {
        fun fromOrderedDayEvents(events: List<TurnstileEvent>): DayInfo {
            var nVisitations: Long = 0
            var totalDuration = Duration.ZERO
            val lastEventPerUser = hashMapOf<LocalPhoneNumber, TurnstileEvent>()
            for (event in events) {
                val previousEvent = lastEventPerUser.put(event.userPhoneNumber, event)
                if (event is TurnstileEvent.Exited) {
                    // Do not check that the previous event is the opposite (i.e. enter for
                    // exit, exit for enter). It should have been checked on earlier stages.
                    // Also it's statistics, so we don't care as much.
                    val dayStart = event.timeCreated.truncatedTo(ChronoUnit.DAYS)
                    val enterTime = previousEvent?.timeCreated ?: dayStart
                    totalDuration += Duration.between(enterTime, event.timeCreated)
                    nVisitations += 1
                }
            }
            for (lastEvent in lastEventPerUser.values) {
                if (lastEvent is TurnstileEvent.Entered) {
                    val dayEnd = lastEvent.timeCreated.truncatedTo(ChronoUnit.DAYS) + Period.ofDays(1)
                    totalDuration += Duration.between(lastEvent.timeCreated, dayEnd)
                }
            }

            return DayInfo(nVisitations, totalDuration)
        }
    }
}

data class Averages(val averageVisitations: Double, val averageVisitDuration: Duration)