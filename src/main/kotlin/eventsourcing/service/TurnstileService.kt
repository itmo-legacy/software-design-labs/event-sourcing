package eventsourcing.service

import eventsourcing.domain.LocalPhoneNumber
import eventsourcing.domain.TurnstileEvent
import eventsourcing.exception.TurnstileException
import eventsourcing.repository.TurnstileEventRepository
import org.springframework.stereotype.Component
import java.time.LocalDateTime

@Component
class TurnstileService(
    val subscriptionService: SubscriptionService,
    val eventRepository: TurnstileEventRepository,
) {
    private fun verifyActiveSubscription(userPhoneNumber: LocalPhoneNumber, timeNow: LocalDateTime) {
        val subscription = subscriptionService.findSubscription(userPhoneNumber)
            ?: throw TurnstileException(userPhoneNumber, "no subscription")
        if (subscription.expirationTime < timeNow) {
            throw TurnstileException(userPhoneNumber, "no active subscription")
        }
    }

    fun registerEnter(userPhoneNumber: LocalPhoneNumber) {
        val lastEvent = eventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhoneNumber)
        val version = when (lastEvent) {
            null -> 0
            is TurnstileEvent.Entered -> throw TurnstileException(userPhoneNumber, "can't enter twice without exiting")
            is TurnstileEvent.Exited -> lastEvent.version + 1
        }
        val event = TurnstileEvent.Entered(userPhoneNumber, version)
        verifyActiveSubscription(userPhoneNumber, event.timeCreated)
        eventRepository.save(event)
    }

    fun registerExit(userPhoneNumber: LocalPhoneNumber) {
        val lastEvent = eventRepository.findFirstByUserPhoneNumberOrderByVersionDesc(userPhoneNumber)
        val version = when (lastEvent) {
            null, is TurnstileEvent.Exited ->
                throw TurnstileException(userPhoneNumber, "can't exit without entering first")
            is TurnstileEvent.Entered -> lastEvent.version + 1
        }
        val event = TurnstileEvent.Exited(userPhoneNumber, version)
        verifyActiveSubscription(userPhoneNumber, event.timeCreated)
        eventRepository.save(event)
    }
}